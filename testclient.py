from socket import *

HOST = '127.0.0.1'
PORT = 1498

sock = socket()
sock.connect((HOST,PORT))

try:
    while True:
        print(sock.recv(1024).decode())
        sock.send(input().encode())
except Exception as e:
    print(e)
finally:
    sock.close()