from random import *
import time

GAME_MENU = """\
Do you want to earn money? So go to the campaign and earn it
1. Regulations
2. Go campaign!
3. Return"""

REGULATIONS = """\
- Earn money for killing the EVIL monsters.
 -> (To kill an monster - "kill {name_monster}") <-
- On the way, you will meet with the HARMLESS animals. 
  After killing one of these animals, you will be sent back.
 -> (To skip an animal - "Skip {name_animal}") <-
- Firstly you will be earning few money, 
  but the more you play, the more money you earn for killing.
- The answer time is limited! The more you play, the less answer time.
- To QUIT the game - "0"

1. Go campaign!
2. Return"""

end = 0
kof = 0
Class = ()
Name_k = ()
score = 0
time_for_answer = 20000

NAME_ANIMALS = (['Djon','Lutor','None','Johan','Roman','Chloya',
'Loly','Valli','Sung','Ronald','Jackie','Steve','Opal',"Grigory",
"Arnold",'Kris',"Karl","R2-D3","Dreg"])

def Regulations(end,kof,score,time_for_answer):
  print (REGULATIONS)
  text = input()
  if text == '1':
    Game(text,end,kof,score,time_for_answer)
    Game_menu()
  elif text == '2':
    Game_menu()

def Game(text_k_s,end,kof,score,time_for_answer):
  while text_k_s != "0":
    Class = choice(["Monster","Animal"])
    Name_k = choice(NAME_ANIMALS)
    print ("You met the -",Class,Name_k)
    kof += 0.5

    if Class == "Monster":
      if time_for_answer >= 1000:
        time_for_answer -= 700

      start_time = time.time()
      text_k_s = input()
      finish_time = time.time()
      
      if ((finish_time - start_time)*1000) <= time_for_answer:
        while (text_k_s != "Kill {}".format(Name_k)) or (text_k_s != "Skip {}".format(Name_k)):
          if text_k_s == "0":
            end = 1
            break
          if text_k_s == "Kill {}".format(Name_k):
            score += kof
            break
          elif text_k_s == "Skip {}".format(Name_k):
            break
          text_k_s = input()
      else:
        end = 1
        print (">Time is up!")

    elif Class == "Animal":
      start_time = time.time()
      text_k_s = input()
      finish_time = time.time()
      
      if ((finish_time - start_time)*1000) <= time_for_answer:
        while (text_k_s != "Skip {}".format(Name_k)) or (text_k_s != "Kill {}".format(Name_k)):
          if text_k_s == "0":
           end = 1
           break
          if text_k_s == "Kill {}".format(Name_k):
           end = 1
           break
          elif text_k_s == "Skip {}".format(Name_k):
           break
          text_k_s = input()
      else:
        end = 1
        print (">Time is up!")

    if end == 1:
        print("For the campaign you earned >>",score,"<<")
        break

def Game_menu():
  print(GAME_MENU)

Game_menu()
text = input()
while text !=3:
  if text == '1':
    Regulations(end,kof,score,time_for_answer)
  elif text == '2':
    Game(text,end,kof,score,time_for_answer)
    Game_menu()
  elif text == '3':
    break
  text = input()