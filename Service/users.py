import time
from base64 import b85encode as hashpwd

class UserManager(dict):
    def __init__(self, server):
        super.__init__()
        self.server = server
        self.default_user = User(None, 'admin', 'admin')
    
    def sign_up(self, client):
        client.menu.send_msg('enter username')
        username = client.menu.get_msg()
        if not self.get(username) is None:
            client.menu.send_msg('user with such name is already registered')
            return
        client.menu.send_msg('enter password')
        password = client.menu.get_msg()
        self[username] = User(client, username, password)
        self[username].alliance = server.database[Alliance].default_alliance
        client.user = self.get(username)

    def sign_in(self, client):
        client.menu.send_msg('enter username')
        username = client.menu.get_msg()
        if self.get(username) is None:
            client.menu.send_msg('user with such name has not been registered') # check my inglish pls
            return
        client.menu.send_msg('enter password')
        password = client.menu.get_msg()
        if self.get(username).check_password(password):
            client.user = self.get(username)
            client.menu.send_msg('you\'ve successfully logged in')
        else:
            client.menu.send_msg('password incorrect')

    def list_all_users(self, client):
        client.menu.prettyoutput(self) # dodelat'

class User(object):
    def __init__(self, client, name, passwd):
        self.client = client
        self.name = name
        self.password_hash = hashpwd(passwd.encode()).decode() # ?? wat?
        self.register_time = time.gmtime()
        self.alliance = None
 
    def check_password(self, passwd):
        return hashpwd(passwd.encode()).decode() == self.password_hash # 'wat' too

    def __cmp__(self, other):
        return self.client == other.client # so, one user can log in from one machine
        # hm hm hm