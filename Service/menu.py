class Page(object):
    def __init__(self, text, name, actions = {}, father = None):
        self.text = text
        self.actions = actions
        self.father = father
        self.children = {}
        self.name = name
        father.add_child(self)
    def add_child(self, page):
        page.father = self
        self.children[page.name] = page
    def process(self, key):
        toDo = self.actions.get(key)
        if toDo is not None:
            toDo()
        else:
            print("Error: functor by key '{}' is not found".format(key))
            print(self.actions)

class Menu(object):
    def __init__(self,root_page,client):
        self.client = client
        self.pages = {}
        self.pages['root'] = root_page
        self.current_page = root_page
    def add_child(self, page, father_name = None):
        if father_name is None:
            father_name = self.current_page.name
        self.pages[page.name] = page
        self.pages.get(father_name).add_child(page)

    def send_msg(self, msg):
        self.client.sock.send(msg.encode())

    def get_msg(self):
        return self.sock.recv(1024).decode().strip()

    def enter_menu(self, name):
        self.current_page = self.current_page.children[name]
    def exit_menu(self):
        self.current_page = self.current_page.father
        
    def process(self,key):
        self.current_page.process(key)