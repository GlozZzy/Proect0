from Service import alliance
from Service import users
from Service import items
from Service import client

class Database(dict):
    def __init__(self, server):
        super.__init__()
        self.server = server
        self[User] = UserManager(server)
        self[Client] = ClientManager(server)
        self[Alliance] = AllianceManager(server)
        self[Item] = ItemManager(server)