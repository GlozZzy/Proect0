from Service import menu
import threading

class ClientManager(dict):
    def __init__(self, server):
        super.__init__()
        self.server = server

    def add_client(self):
        conn, addr = self.server.sock.accept()
        self[addr] = Client(conn, addr, self)

    def clean_up(self):
        inactive_clients = []
        for client in self:
            if not self[client].isAlive():
                inactive_clients.append(client)
        for client in inactive_clients:
            self.pop(client)

class Client(threading.Thread):
    def __init__(self, sock, addr, server): # i want to create a default user object for unloginned clients for better project arcitecture
        super().__init__()
        self.sock = sock
        self.addr = addr
        self.server = server
        self.user = server.database[User].default_user
        self.init_menu()
        self.finish = False
        self.start()

    def init_menu(self):
        root = menu.Page("Good afternoon!\nPress:\n1 to sign up\n2 to sign in", 'root', { '1': self.server.database[User].sign_up, '2': self.server.database[User].sign_in })
        user_welcome = menu.Page("Hello {}! What did you want to do?\n0. Sign IN Alliance\n1. Create Alliance\n2. List Alliance\n4. Logout", 'user_welcome', { '0': self.server.database[Alliance]., }, root)
        alliance_welcome = menu.Page("Welcome in {}! What did you want to do?\n1. Cheak gold 'Alliance'\n2. Buy Item\n3. Inventory 'Alliance'\n4. I need a more gold\n5. Create own item\n6. Logout", 'alliance_welcome',{}, user_welcome)
        self.menu = menu.Menu(root)

    def __cmp__(self, other):
        return self.addr == other.addr

    def disconnect(self):
        self.finish = True

    def run(self):
        while True:
            self.send_msg(self.menu.current_page.text)
            input = get_msg()
            self.menu.process(input)
            if self.finish:
                sock.close()
                break