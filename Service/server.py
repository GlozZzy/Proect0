import socket
from Service import database

class Server(object):
    def __init__(self, host, port):
        self.sock=socket.socket()
        self.sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        self.sock.bind((host, port))
        self.sock.listen(64)
        self.database = Database(self)

    def work(self):
        while True:
            self.database[Client].add_client()
            self.database[Client].clean_up()