import time
from base64 import b85encode as hashpwd

class AllianceManager(dict):
    def __init__(self, server):
        super.__init__()
        self.server = server
        self.default_alliance = Alliance('default', 'default', 'admin')

    def create_alliance(client):
        if client.user == server.database[User].default_user:
            client.menu.send_msg('log in before alliance establishing')
            return
        client.menu.send_msg('enter alliance name')
        alliance_name = client.menu.get_msg()
        if not self.get(alliance_name) is None:
            client.menu.send_msg('alliance with such name already exists')
            return
        client.menu.send_msg('enter alliance password')
        alliance_password = client.menu.get_msg()
        self[alliance_name] = Alliance(alliance_name, alliance_password, client.user.name)

    def get_in(client):
        if client.user == server.database[User].default_user:
            client.menu.send_msg('log in pls')
            return

    def list_all_alliances(client):
        client.menu.prettyout # dodelat'
 
class Alliance(object):
    def __init__(self, alliance_name, passwd, owner_name):
        self.alliance_name = alliance_name
        self.password = hashpwd(passwd.encode()).decode()
        self.register_time = time.gmtime()
        self.owner = owner_name
        self.user_list = []
        self.add_user(self.owner)
 
    def check_password(self, passwd):
        return hashpwd(passwd.encode()).decode() == self.password

    def add_user(self, user_name): #add interface
        if user_name not in self.user_list:
            self.user_list.append(user_name)